import React from 'react';
import { FiArrowRight } from 'react-icons/fi';
import { Link } from 'react-router-dom';
import '../style/landing.css';
import logoImg from '../../assets/images/pokeball.png';


function Landing(){
    return (
        <div id="page-landing">
          <div className="content-wrapper">
            <img src={logoImg} width = "180" height="120" alt="Pokedex" />
    
            <Link to="/app" className="enter-app">
              <FiArrowRight size={26} color="rgba(0,0,0, 0.8)" />
            </Link>
          </div>
        </div>
      );
    }
    
    export default Landing;