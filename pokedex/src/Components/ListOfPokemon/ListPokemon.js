import React, { useState, useEffect } from 'react';
import PokemonThumbnail from '../Pokemon/PokemonThumbnail';
import { Link } from 'react-router-dom';
import "../style/List.css";
import { FiArrowLeft } from "react-icons/fi";

function ListPokemon(){
    const [allPokemons, setAllPokemons] = useState([]);
    const [loadMore, setLoadMore] = useState(`https://pokeapi.co/api/v2/pokemon?limit=30`);

    const getAllPokemons = async () => {
    const res = await fetch (loadMore);
    const data = await res.json();
    console.log(data);

    setLoadMore(data.next);
    function createPokemonObject (result) {
        result.forEach( async (pokemon) => {
            const res = await fetch (`https://pokeapi.co/api/v2/pokemon/${pokemon.name}`)
            const data = await res.json();

            setAllPokemons(currentList => [...currentList, data])

        } )
        }
        createPokemonObject(data.results);
        await console.log(allPokemons)
    }
    useEffect(() => {
        getAllPokemons();
    }, [])
    return (
        <div className="app">
        <Link to="/app">
            <FiArrowLeft className="back" size={20} color="#FFF" />
        </Link>
        <h1>List of Pokemon</h1>
        <div className="pokemon">
            <div className="all">
                {allPokemons.map ((pokemon, index) => 
                <PokemonThumbnail
                 id={pokemon.id}
                 name={pokemon.name}
                 image={pokemon.sprites.front_default}
                 type={pokemon.types[0].type.name}
                 key={index}
                  />
                )}
            </div>
            <button className="load" onClick={() => getAllPokemons()}>Load more</button>
        </div>

        </div>
    );
}

export default ListPokemon;