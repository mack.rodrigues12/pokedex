import React from 'react';
import { Route, Switch } from 'react-router-dom';

import Landing from '../Landing/Landing';
import SearchPokemon from '../SearchPokemon/SearchPokemon';
import List from '../ListOfPokemon/ListPokemon';

export default function Pokedex() {
  return (
    <Switch>
      <Route path="/" exact component={Landing}></Route>
      <Route exact path="/app" component={SearchPokemon}></Route>
      <Route path="/app/list" component={List}></Route>
    </Switch>
  );
}
