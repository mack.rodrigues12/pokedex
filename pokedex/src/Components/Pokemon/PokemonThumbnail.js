import React from 'react';
import "../style/List.css";

const PokemonThumbnail = ({id, name, image, type}) =>{

    const style = `thumb ${type}`
    return (
        <div className={style}>
            <div className="number">
            <small>#0{id}</small>
            </div>  
            <img src={image} alt={name}/>
            <div className="detail">
                <h3>{name}</h3>
                <small>Type: {type} </small>
            </div>
        </div>
    )
}
export default PokemonThumbnail;