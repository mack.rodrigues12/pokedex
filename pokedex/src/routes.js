import React from 'react';
import { BrowserRouter } from 'react-router-dom';

import Pokedex from './Components/Routes/Pokedex';


function Routes() {
  return (
    <BrowserRouter>
      <Pokedex />
    </BrowserRouter>
  );
}

export default Routes;
